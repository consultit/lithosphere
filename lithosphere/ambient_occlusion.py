# -*- coding: utf-8 -*-

"""
    :copyright: 2010 by Florian Boesch <pyalot@gmail.com>.
    :license: GNU AGPL v3 or later, see LICENSE for more details.
"""
from __future__ import with_statement

from halogen import Label, Column
from .util import nested, quad, connect, LabelSlider
from gletools import Texture, Framebuffer, Sampler2D
from pyglet.gl import *

class AmbientOcclusion(object):
    def __init__(self, application):
        self.application = application

        col = Column().add_class('ambient_occlusion').append_to(application.viewport)
        Label('Ambient Occlusion').append_to(col)
        self.scale = LabelSlider('Strengh', start=0.5).append_to(col)
        self.scale.slider.on_change = self.change

        self.blurred = Texture(application.width, application.height, GL_RGBA32F, clamp='st')
        self.result = Texture(application.width, application.height, GL_RGBA32F, clamp='st')

        self.gaussian = application.shader('gaussian.frag')
        self.gaussian.vars.texture = Sampler2D(GL_TEXTURE0)
        self.gaussian.vars.filter_weight = Sampler2D(GL_TEXTURE1)
        self.gaussian.vars.offsets = 1.0/application.width, 1.0/application.height

        self.ambient_occlusion = application.shader('ambient_occlusion.frag')
        self.ambient_occlusion.vars.heightfield = Sampler2D(GL_TEXTURE0)
        self.ambient_occlusion.vars.blurred = Sampler2D(GL_TEXTURE1)

        self.changed = True

    def open(self, data):
        if data is not None:
            self.scale.value = data

    def save(self):
        return self.scale.value

    def change(self, value):
        self.changed = True

    def update(self):
        if self.changed:
            heightfield = self.application.terrain.get_source()
            result = self.result

            if heightfield:

                gaussian = self.gaussian
                ambient_occlusion = self.ambient_occlusion
                tmp = self.application.temp
                blurred = self.blurred
                    
                self.apply(gaussian, blurred, heightfield)

                for _ in range(10):
                    self.apply(gaussian, tmp, blurred)
                    self.apply(gaussian, blurred, tmp)

                self.ambient_occlusion.vars.scale = self.scale.value * 1000.0
                self.apply(ambient_occlusion, result, heightfield, blurred)
            else:
                fbo = self.application.framebuffer
                fbo.textures[0] = result
                view = self.application.processing_view
                with nested(view, fbo):
                    glColor4f(1.0, 1.0, 1.0, 1.0)
                    quad(result.width, result.height)

            self.changed = False
    
    def apply(self, shader, target, *sources):
        view = self.application.processing_view
        
        for i, source in enumerate(sources):
            source.unit = GL_TEXTURE0 + i

        fbo = self.application.framebuffer
        fbo.textures[0] = target

        with nested(view, fbo, shader, *sources):
            quad(target.width, target.height)
