# -*- coding: utf-8 -*-

"""
    :copyright: 2010 by Florian Boesch <pyalot@gmail.com>.
    :license: GNU AGPL v3 or later, see LICENSE for more details.
"""

from pyglet.gl import *
from halogen import Canvas
from array import array

class LineCanvas(Canvas):
    def __init__(self):
        Canvas.__init__(self, id='canvas')
        self.connectors = []
        self.buffer = array('f')
        
    def on_draw(self):
        glLineWidth(3.0)
        glTexCoord2f(
            self.root.resources.white.left,
            self.root.resources.white.bottom,
        )
        glColor4f(255.0/255.0, 184.0/255.0, 48.0/255.0, 0.7)
        self.update_buffer()
        pyglet.graphics.draw(len(self.buffer)/2, GL_LINES,
            ('v2f', self.buffer),
        )

    def update_buffer(self):
        for i, connector in enumerate(self.connectors):
            start = connector.output.rect.center
            end = connector.rect.center
            self.buffer[i*4+0] = start.x
            self.buffer[i*4+1] = start.y
            self.buffer[i*4+2] = end.x
            self.buffer[i*4+3] = end.y

    def add(self, connector):
        self.connectors.append(connector)
        self.buffer.extend([0,0,0,0])

    def remove(self, connector):
        self.connectors.remove(connector)
        del self.buffer[-4:]

